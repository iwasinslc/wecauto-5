<?php


namespace App\Facades;

use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\TransferService;
use Illuminate\Support\Facades\Facade;

/**
 * Class TransferF
 * @package App\Facades
 * @method static create(Wallet $sender_wallet, Wallet $receiver_wallet, float $amount, $with_confirmation = true)
 * @method static confirm(Transaction[] $transactions)
 * @method static confirmByCode(string $confirmation_code)
 *
 * @see TransferService
 */
class TransferF extends Facade
{
    protected static function getFacadeAccessor()
    {
        return TransferService::class;
    }
}