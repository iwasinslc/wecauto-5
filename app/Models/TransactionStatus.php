<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    use HasFactory;

    const STATUS_CREATED = 0;
    const STATUS_APPROVED = 1;
    const STATUS_CONFIRMED_BY_EMAIL = 2;
    const STATUS_REJECTED = 3;
    const STATUS_ERROR = 4;

    public $timestamps = false;

    public function withdrawTransactions() {
        return $this->hasMany(Withdraw::class, 'status_id', 'id');
    }
}
