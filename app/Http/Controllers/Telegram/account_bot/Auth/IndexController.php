<?php
namespace App\Http\Controllers\Telegram\account_bot\Auth;

use App\Http\Controllers\Controller;
use App\Models\Telegram\TelegramBotEvents;
use App\Models\Telegram\TelegramBots;
use App\Models\Telegram\TelegramBotScopes;
use App\Models\Telegram\TelegramUsers;
use App\Models\Telegram\TelegramWebhooks;
use App\Modules\Messangers\TelegramModule;

class IndexController extends Controller
{
    /**
     * @param TelegramWebhooks $webhook
     * @param TelegramBots $bot
     * @param TelegramBotScopes $scope
     * @param TelegramUsers $telegramUser
     * @param TelegramBotEvents $event
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function index(TelegramWebhooks $webhook,
                          TelegramBots $bot,
                          TelegramBotScopes $scope,
                          TelegramUsers $telegramUser,
                          TelegramBotEvents $event)
    {
        TelegramModule::setLanguageLocale($telegramUser->language);

        $user = $telegramUser->user;


        $message = view('telegram.account_bot.auth.index', [
            'webhook'      => $webhook,
            'bot'          => $bot,
            'scope'        => $scope,
            'telegramUser' => $telegramUser,
        ])->render();

        if (config('app.env') == 'develop') {
            \Log::info('Prepared VIEW and message for bot:<hr>'.$message);
        }

        $keyboard = [
            [
                ['text' => __('Login'), 'callback_data' => '/enter_login'],
                ['text' => __('Registration'), 'callback_data' => 'login'],
            ],
        ];



        try {
            $telegramInstance = new TelegramModule($bot->keyword);
            $telegramInstance->sendMessage(
                $telegramUser->chat_id,
                $message,
                'HTML',
                true,
                false,
                null,
                [
                    'inline_keyboard' => $keyboard,
                    'resize_keyboard' => true,
                ],
                $scope,
                'keyboard'
            );
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response('ok');
        }

        return response('ok');
    }
}
