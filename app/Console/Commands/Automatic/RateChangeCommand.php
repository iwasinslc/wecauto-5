<?php
namespace App\Console\Commands\Automatic;

use App\Jobs\AccrueDeposit;
use App\Jobs\CloseDeposit;
use App\Models\DepositQueue;
use App\Models\PriceUps;
use App\Models\RateStatistic;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use Illuminate\Console\Command;

/**
 * Class DepositQueueCommand
 * @package App\Console\Commands\Automatic
 */
class RateChangeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rate:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run deposits queues.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle()
    {
        $start_time = now()->subDay()->startOfDay()->addHour()->toDateTimeString();
        $end_time = now()->subDay()->endOfDay()->addHour()->toDateTimeString();
        $types = [
            TransactionType::getByName('exchange_buy')->id,
            TransactionType::getByName('exchange_sell')->id,
        ];

        $transactions = Transaction::whereIn('type_id', $types)->where('created_at','>=' ,$start_time)->where('created_at','<=' ,$end_time)->get();

        if ($transactions->count()==0)
        {
            $price = rate('ACC', 'USD');

            $percent = $price*(5)*0.01;
            $price=$price-$percent;



            /**
             * @var PriceUps $row
             */
            $row = PriceUps::where('price', '<=', $price)->orderBy('price', 'desc')->first();

            if ($row===null)
            {
                $row = PriceUps::orderBy('price', 'asc')->first();
            }

            $price_amount = 0;

            if ($price-$row->price>=0)
            {
                $price_amount = (($price-$row->price)/$row->price)*$row->amount;
            }


            if ($price<=0.1)
            {
                $price = 0.1;

                $price_amount = 0;
            }





            Setting::setValue('acc_to_usd', $price);

            Setting::setValue('price_up_amount', $price_amount);

            return 0;
        }




    }
}
