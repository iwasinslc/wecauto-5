<?php
return [
    'emails' => [
        'spam_protection' => 'Too many emails from you',
        'sent_successfully' => 'Mail was sent successfully',
        'unable_to_send' => 'Unable to send email',

        'request' => [
            'email_required' => 'Email is required',
            'email_max' => 'Maximum email length 255 symbols',
            'email_email' => 'Wrong email format',

            'text_required' => 'Text is required',
            'text_min' => 'Minimum text length 10 symbols',
        ],
    ],
    'transaction_types' => [
        'enter' => 'Top up balance',
        'withdraw' => 'Withdraw',
        'bonus' => 'Bonus',
        'partner' => 'Affiliate commission',
        'dividend' => 'Deposit earnings',
        'create_dep' => 'Created deposit',
        'close_dep' => 'Closed deposit',
        'penalty' => 'Penalty',
        'exchange_commission'=>'Enter Exchange',
        'transfer_send' => 'Send Transfer',
        'transfer_receive' => 'Receive Transfer',
        'licence_cash_back' => 'Licence CashBack',
        'swap_withdraw' => 'Send Transfer in ACC',
        'buy_wec' => 'Buy Wec',
        'charity' => 'Charity',
        'stake_wec' => 'Stake WEC',
        'exempt_wec' => 'Exempt WEC',
        'add_accelerators' => 'Add Accelerators',
        'remove_accelerators' => 'Remove Accelerators',
        'buy_license' => 'Buy Licence',
        'exchange_order' => 'Exchange Order',
        'exchange_buy' => 'Buy',
        'exchange_sell' => 'Sell',
        'exchange_close_order' => '',
    ],

    'bonus' => [
        'send'                  => 'Send bonus',
        'transaction_comment'   => 'Transaction comment',
        'transaction_batch_id'  => 'Transaction batch_id',
    ],

    'penalty' => [
        'send'  => 'Send penalty',
    ],

    'amount'    => 'Amount',
    'send'      => 'Send',
    'close'     => 'Close',

];
