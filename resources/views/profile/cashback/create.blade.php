@extends('layouts.profile')
@section('title', 'Подать заявку')

@section('content')
    <div class="cashback">
        <!---->
        <section>
            <div class="container">
                <ul class="tabs-navigation">
                    <li><a href="{{ route('profile.cashback.conditions') }}">Как получить Cash Back?</a>
                    </li>
                    <li class="is-active"><a href="{{ route('profile.cashback.create') }}">Подать заявку</a>
                    </li>
                    <li><a href="{{ route('profile.cashback.index') }}">Мои заявки</a>
                    </li>
                </ul>
            </div>
        </section>
        <section>
            <div class="container">
                @if(isCashbackRequestSent())
                    <h2 class="lk-title">Заяка уже подана</h2>
                @else
                <h2 class="lk-title">Подать заявку
                </h2>
                <div class="content-block">
                    <form accept-charset="utf-8" enctype="multipart/form-data" method="POST" action="{{route('profile.cashback.store')}}">
                        @csrf
                        <div class="cashback-form">
                            <div class="cashback-form__top">
                                <div class="cashback-form__top-col-left">
                                    <div class="row">
                                        <div class="col">
                                            <div class="field field--gray">
                                                <label>Ваш логин в проекте WecAuto:</label>
                                                <input type="text" value="{{$user->login}}" name="login" placeholder="Логин" disabled>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="field field--gray">
                                                <label>Ваша почта в проекте WecAuto:</label>
                                                <input type="text" value="{{$user->email}}" name="email" placeholder="mail@gmail.com" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="field field--gray @error('telegram') field--error @enderror">
                                                <label>Ваш Telegram:</label>
                                                <input type="text" name="telegram" value="{{ old('telegram') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cashback-form__top-col-right"><a class="banner-video" href="#" data-fancybox="data-fancybox"><span class="banner-video__icon"><img src="/assets/images/cashback/video.svg" alt=""></span>
                                        <h5 class="banner-video__subtitle">Смотреть видео
                                        </h5>
                                        <h4 class="banner-video__title">«Как подать заявку на CashBack»
                                        </h4></a>
                                </div>
                            </div>
                            <div class="field field--gray">
                                <label>Прикрепите документы (свидетельство о регистрации ТС, договор купли-продажи ТС (pdf, tiff, png, jpg, jpeg, не более 5 MB)):</label>
                                <div class="files-uploader">
                                    <!-- is-loading-->
                                    <label class="files-uploader__button">
                                        <input type="file" accept="application/pdf,image/tiff,image/png,image/jpg,image/jpeg" id="file-input" name="documents[]"><span>Добавьте файл</span>
                                    </label>
                                    <ul id="output_file_list" class="files-uploader__list">
                                    </ul>
                                </div>
                            </div>
                            <div class="field field--gray @error('video_link') field--error @enderror">
                                <label>Ссылка на видео:</label>
                                <input type="text"  value="{{old('video_link')}}" name="video_link" placeholder="Яндекс Диск или Google Drive">
                            </div>
                            <div class="field field--gray @error('comment') field--error @enderror">
                                <label>Комментарий:</label>
                                <textarea name="comment">{{old('comment')}}</textarea>
                            </div>
                            <div class="field field--gray @error('confirm') field--error @enderror">
                                <label class="checkbox">
                                    <input type="checkbox" {{ old('confirm') ? 'checked' : '' }} name="confirm"><span>Я согласен на <a href="{{ route('customer.agreement') }}">обработку персональных данных</a>, использование, размещение и монтаж видео в рекламных целях на площадках и ресурсах WTP.</span>
                                </label>
                            </div>
                            <button class="btn btn--warning">Подать заявку
                            </button>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </section>
    </div>
@endsection

@push('load-scripts')
    <script>
        $(document).ready(function(){
            $('#file-input').on('change', function(e){ //on file input change
                var li = $('<li></li>'),
                    block = $('<div class="file-item"></div>'),
                    file_name = e.target.files[0].name,
                    input = $('#file-input').clone(),
                    ul = $('#output_file_list');

                if (ul.children().length < 2) {
                    // Clone file input
                    input.removeAttr('id');
                    input.attr('hidden', 'hidden');
                    input.attr('name', 'files[]');

                    block.append('<span>' + file_name + '</span><div class="file-item__remove"></div>');

                    li.append(input);
                    li.append(block);
                    ul.append(li);
                }
                input.value = ''; // remove file from input
            });

            $(document).on('click', '.file-item__remove', function() {
                $(this).closest('li').remove();
            })
        });
    </script>
@endpush
