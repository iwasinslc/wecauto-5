@if($wallet->external!=null)
    {{__('Current wallet')}}  - {{$wallet->external}}. {{__('If you want to change it please enter new one')}}
    @else
    {{__('Enter wallet address')}}
@endif